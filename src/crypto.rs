use crate::cli;
use std::fs;
use std::io::prelude::*;
use sodiumoxide::crypto::secretbox;
use sodiumoxide::crypto::secretbox::xsalsa20poly1305::Key;
use sodiumoxide::crypto::secretbox::xsalsa20poly1305::KEYBYTES;
use sodiumoxide::crypto::secretbox::xsalsa20poly1305::Nonce;
use sodiumoxide::crypto::secretbox::xsalsa20poly1305::NONCEBYTES;
use sodiumoxide::randombytes::randombytes_into;

fn init() -> bool {
	match sodiumoxide::init() {
		Ok(_) => return true,
		Err(_) => cli::exit_error("Could not securely initialize sodiumoxide.".to_string()),
	};
	return false;
}

pub fn encrypt(args: Vec<String>) {
	if init() {
		let input_file_content = read_from_argument_option(args.clone(), "-i", "--input");
		let key = load_key(args.clone(), "-k", "--key");
		let mut nonce_bytes = [0; NONCEBYTES];
		randombytes_into(&mut nonce_bytes);
		let nonce = Nonce(nonce_bytes);
		let mut cipher_text_vector = secretbox::seal(&input_file_content[..], &nonce, &key);
		let mut output_vector: Vec<u8> = Vec::new();
		output_vector.extend(nonce_bytes.iter());
		output_vector.append(&mut cipher_text_vector);
		write_to_argument_option(args, &output_vector[..], "-o", "--output");
	}
}

pub fn decrypt(args: Vec<String>) {
	if init() {
		let input_file_content = read_from_argument_option(args.clone(), "-i", "--input");
		if input_file_content.len() < NONCEBYTES {
			cli::exit_error("File too small to contain nonce.".to_string());
		}
		let key = load_key(args.clone(), "-k", "--key");
		let mut nonce_bytes = [0; NONCEBYTES];
		for byte in 0..NONCEBYTES {
			nonce_bytes[byte] = input_file_content[byte];
		}
		let nonce = Nonce(nonce_bytes);
		match secretbox::open(&input_file_content[NONCEBYTES..], &nonce, &key) {
			Ok(plain_text_vector) => {
				write_to_argument_option(args, &plain_text_vector[..], "-o", "--output");
			},
			Err(_) => cli::exit_error("Could not decrypt file with given key.".to_string()),
		};
	}
}

pub fn write_key(args: Vec<String>) {
	if init() {
		let mut key_bytes = [0; KEYBYTES];
		randombytes_into(&mut key_bytes);
		write_to_argument_option(args, &key_bytes, "-o", "--output");
	}
}

fn load_key(args: Vec<String>, short: &str, long: &str) -> Key {
	let input_file_name = get_argument_value(args.clone(), short, long);
	let mut input_file_content = [0; KEYBYTES];
	match fs::File::open(input_file_name.clone()) {
		Ok(mut input_file) => {
			match input_file.read(&mut input_file_content) {
				Ok(_) => return Key(input_file_content),
				Err(_) => cli::exit_error(format!("Error reading key file: {}", input_file_name)),
			};
		},
		Err(_) => cli::exit_error(format!("Error opening key file: {}", input_file_name)),
	};
	return Key(input_file_content);
}

fn read_from_argument_option(args: Vec<String>, short: &str, long: &str) -> Vec<u8> {
	let input_file_name = get_argument_value(args.clone(), short, long);
	let mut input_file_content = Vec::new();
	match fs::File::open(input_file_name.clone()) {
		Ok(mut input_file) => {
			match input_file.read_to_end(&mut input_file_content) {
				Ok(_) => return input_file_content,
				Err(_) => cli::exit_error(format!("Error reading input file: {}", input_file_name)),
			};
		},
		Err(_) => cli::exit_error(format!("Error opening input file: {}", input_file_name)),
	};
	return input_file_content;
}

fn write_to_argument_option(args: Vec<String>, output_file_data: &[u8], short: &str, long: &str) -> bool {
	let output_file_name = get_argument_value(args.clone(), short, long);
	match fs::File::create(output_file_name.clone()) {
		Ok(mut output_file) => {
			match output_file.write(output_file_data) {
				Ok(_) => return true,
				Err(_) => cli::exit_error(format!("Error writing output file: {}", output_file_name)),
			};
		},
		Err(_) => cli::exit_error(format!("Error opening output file: {}", output_file_name)),
	};
	return false;
}

fn get_argument_value(args: Vec<String>, short: &str, long: &str) -> String {
	let mut argument_index = 0;
	match args.iter().position(|arg| arg == short || arg == long) {
		Some(index) => {
			argument_index = index + 1;
			if args.len() <= argument_index {
				cli::exit_error(format!("No value specified for option {}", args[argument_index - 1]));
			}
		},
		None => cli::exit_error(format!("Neither argument {} or {} found.", short, long)),
	}
	return args[argument_index].clone();
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn gets_proper_value() {
		let short_option = "-o";
		let long_option = "--output";
		let value = "test_value";
		let args_with_short_option = vec![short_option.to_string(), value.to_string()];
		assert_eq!(value, get_argument_value(args_with_short_option, short_option, long_option));
		let args_with_long_option = vec![long_option.to_string(), value.to_string()];
		assert_eq!(value, get_argument_value(args_with_long_option, short_option, long_option));
	}
}
