mod longtext;
use std::process;

pub fn usage() {
	exit_error("Usage: rustlock [encrypt|decrypt|genkey|help|license] [-iok]".to_string());
}

pub fn help() {
	println!("Usage: rustlock [command] [options]");
	println!("Commands:");
	println!("  encrypt\t\tEncrypts a file with the specified key file.");
	println!("  decrypt\t\tDecrypts an encrypted file with the specified key file.");
	println!("  genkey\t\tGenerates a new key file which can be used to encrypt and decrypt files.");
	println!("  help\t\t\tDisplays this help message.");
	println!("  license\t\tDisplays information about the license of rustlock and third party libraries it uses.");
	println!("Options:");
	println!("  -i, --input\t\tinput file");
	println!("  -o, --output\t\toutput file");
	println!("  -k, --key\t\tkey file");
}

pub fn license() {
	println!("{}", longtext::RUSTLOCK_LICENSE);
	println!("\nThe licenses for third party libraries used in rustlock are included below:");
	for license in longtext::THIRD_PARTY_LICENSES.iter() {
		println!("\n{}", license);
	}
}

pub fn exit_error(print_message: String) {
	println!("{}", print_message);
	process::exit(1);
}
