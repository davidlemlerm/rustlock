mod cli;
mod crypto;
use std::env;

fn main() {
	let args: Vec<String> = env::args().collect();
	match args.len() {
		1 => cli::usage(),
		_ => {
			match args[1].as_str() {
				"encrypt" => crypto::encrypt(args),
				"decrypt" => crypto::decrypt(args),
				"genkey" => crypto::write_key(args),
				"help" => cli::help(),
				"license" => cli::license(),
				_ => cli::usage(),
			}
		},
	}
}
