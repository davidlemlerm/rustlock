# rustlock

Rustlock is a simple CLI utility that can be used to encrypt and decrypt files, and generate encryption keys. It is written in Rust and uses the secretbox functionality of the sodiumoxide cryptography library for encryption, decryption, and the generation of encryption keys and nonces.

## Usage

```
rustlock [encrypt|decrypt|genkey|help|license] [-iok]
```

```
rustlock [command] [options]
Commands:
  encrypt
  decrypt
  genkey
  help
  license
Options:
  -i, --input
  -o, --output
  -k, --key
```

## Security

rustlock has not been audited or verified to be cryptographically secure. It's mainly a small program that I wrote to become more familiar with the Rust programming language. However, the underlying library that rustlock uses, sodiumoxide, primarily serves as Rust bindings for libsodium, an implementation of the NaCl cryptography library, which _is_ well known and used, so any lack of security in rustlock is likely a result of its implementation of sodiumoxide, rather than any underlying cryptography libraries.

## License

rustlock is licensed under the BSD Zero Clause License. Below is a table listing the third party libraries rustlock uses and their licenses:

| Library     | License           |
| ----------- | ----------------- |
| sodiumoxide | MIT or Apache-2.0 |
